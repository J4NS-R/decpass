
#ifndef DECPASS_CONFIGMANAGER_H
#define DECPASS_CONFIGMANAGER_H


#include <string>

class ConfigManager {
public:
    ConfigManager();

    [[nodiscard]] bool isConfigured() const;
    void writeOut();

private:
    std::string app_dir;
    std::string config_file;

    bool configured;
    std::string access_grant;
    std::string bucket;
    long remote_sync_ts = -1;
    long local_sync_ts = -1;

public:
    [[nodiscard]] const std::string &getConfigFile() const;

    [[nodiscard]] const std::string &getAccessGrant() const;

    void setAccessGrant(const std::string &accessGrant);

    [[nodiscard]] const std::string &getBucket() const;

    void setBucket(const std::string &bucket);

    void setConfigured(bool new_configured);

    [[nodiscard]] const std::string &getAppDir() const;

    [[nodiscard]] long getRemoteSyncTs() const;

    void setRemoteSyncTs(long remoteSyncTs);

    long getLocalSyncTs() const;

    void setLocalSyncTs(long localSyncTs);
};


#endif //DECPASS_CONFIGMANAGER_H
