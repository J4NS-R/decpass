#include "ConfigManager.h"
#include "../util/IOHelper.h"
#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <sstream>

using namespace std;

ConfigManager::ConfigManager() {
    char *home = getenv("HOME");
    std::string buf(home);
    buf.append("/.decpass");
    app_dir = buf;

    if (!IOHelper::PathExists(&app_dir)){
        int res = mkdir(app_dir.c_str(), 0700);
        if (res == -1){
            std::cerr << "Failed to create config directory: " << app_dir << std::endl;
        }
    }

    config_file = app_dir + "/config.properties";

    if (!IOHelper::PathExists(&config_file)){
        std::ofstream cfgfile;
        cfgfile.open(config_file, std::ios::out);
        cfgfile << "configured=0" << std::endl;
        cfgfile.close();
        // set config file to only be user-modifiable
        chmod(config_file.c_str(), S_IRUSR | S_IWUSR);
    }

    // Read in config file
    std::fstream cfgfile;
    cfgfile.open(config_file, std::ios::in);
    std::string line;
    while(getline(cfgfile, line)){
        int eq = line.find('=');
        if (eq >= line.length()){
            cerr << "Failed to parse config line: \"" << line << "\"" << endl;
            break;
        }

        // parse property line
        std::string key = line.substr(0, eq);
        std::string val = line.substr(eq+1);

        // can't switch on a string :(
        if (key == "configured"){
            configured = val=="1";
        }else if (key == "access_grant"){
            access_grant = val;
        }else if (key == "bucket") {
            bucket = val;
        }else if (key == "remote_sync_ts"){
            std::stringstream sstr(val);
            sstr >> remote_sync_ts;
        }else if (key == "local_sync_ts"){
            std::stringstream sstr(val);
            sstr >> local_sync_ts;
        }else{
            cerr << "Unknown config item encountered: " << key << endl;
        }
        // more config goes here and in `writeOut()`
    }
    cfgfile.close();

}

void ConfigManager::writeOut() {
    ofstream cfgfile;
    cfgfile.open(config_file, ios::out);
    cfgfile << "configured=" << configured << endl;
    if (configured){
        cfgfile << "access_grant=" << access_grant << endl
            << "bucket=" << bucket << endl
            << "remote_sync_ts=" << remote_sync_ts << endl
            << "local_sync_ts=" << local_sync_ts << endl;
    }
    cfgfile.close();
}

bool ConfigManager::isConfigured() const {
    return configured;
}

const std::string &ConfigManager::getConfigFile() const {
    return config_file;
}

const std::string &ConfigManager::getAccessGrant() const {
    return access_grant;
}

void ConfigManager::setAccessGrant(const std::string &accessGrant) {
    access_grant = accessGrant;
}

const std::string &ConfigManager::getBucket() const {
    return bucket;
}

void ConfigManager::setBucket(const std::string &new_bucket) {
    ConfigManager::bucket = new_bucket;
}

void ConfigManager::setConfigured(bool new_configured) {
    ConfigManager::configured = new_configured;
}

const string &ConfigManager::getAppDir() const {
    return app_dir;
}

long ConfigManager::getRemoteSyncTs() const {
    return remote_sync_ts;
}

void ConfigManager::setRemoteSyncTs(long remoteSyncTs) {
    remote_sync_ts = remoteSyncTs;
}

long ConfigManager::getLocalSyncTs() const {
    return local_sync_ts;
}

void ConfigManager::setLocalSyncTs(long localSyncTs) {
    local_sync_ts = localSyncTs;
}
