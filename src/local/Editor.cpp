
#include <fstream>
#include <iostream>
#include "Editor.h"
#include "../util/GeneralHelper.h"

/**
 * Take in an item, edit it, and returned an edited version (will not mutate input)
 */
SecureItem* Editor::editItem(SecureItem *item) {

    // create temporary file
    const std::string tmpfile_path = "/tmp/decpass_item";
    std::ofstream tmpfile(tmpfile_path, std::ios::out);

    tmpfile << "# Edit anything but the structure. Note section is multiline and starts under the header." << std::endl;
    tmpfile << "Name=" << item->getName() << std::endl;

    bool is_login = item->getType() == SecureItem::ITEM_TYPE::LOGIN;
    if (is_login){
        auto *login = (Login*) item;
        tmpfile << "Username=" << login->getUsername() << std::endl
                << "Password=" << login->getPassword() << std::endl
                << "Notes:" << std::endl;
        GeneralHelper::printLines(login->getNotes(), 0, &tmpfile);

    }else{ // Note
        auto *note = (Note*) item;
        tmpfile << "Note:" << std::endl;
        GeneralHelper::printLines(note->getNote(), 0, &tmpfile);

    }

    tmpfile.close();

    // open and passthrough to editor
    system(("vim "+tmpfile_path).c_str());

    return itemFromFile(is_login, tmpfile_path);

}

SecureItem * Editor::newItem(bool is_login, const std::string &name) {

    // create temporary file
    const std::string tmpfile_path = "/tmp/decpass_item";
    std::ofstream tmpfile(tmpfile_path, std::ios::out);

    tmpfile << "# Edit anything but the structure. Note section is multiline and starts under the header." << std::endl;
    tmpfile << "Name=" << name << std::endl;

    if (is_login){
        tmpfile << "Username=" << std::endl
                << "Password=" << std::endl
                << "Notes:" << std::endl;

    }else{ // Note
        tmpfile << "Note:" << std::endl;

    }

    tmpfile.close();

    // run editor and block
    system(("vim "+tmpfile_path).c_str());

    return itemFromFile(is_login, tmpfile_path);

}

/**
 * @param is_login true if login, false if note
 * @param path of file to read from
 * @return a parsed item or nullptr if there is a formatting issue
 */
SecureItem* Editor::itemFromFile(bool is_login, const std::string &path) {

    // read in edited tmpfile
    std::ifstream tmpfile_in(path, std::ios::in);

    std::string line;
    getline(tmpfile_in, line); // skip first line: comment
    getline(tmpfile_in, line); // Name
    if (line.substr(0, 5) != "Name="){
        std::cerr << "Item format issue. Not saved." << std::endl; return nullptr;
    }
    auto new_name = line.substr(5);
    if (new_name.empty()){
        std::cerr << "No name specified. Not saved." << std::endl; return nullptr;
    }

    SecureItem *new_item;
    if (is_login){
        getline(tmpfile_in, line); // Username
        if (line.substr(0, 9) != "Username="){
            std::cerr << "Item format issue. Not saved." << std::endl; return nullptr;
        }
        auto new_username = line.substr(9);

        getline(tmpfile_in, line); // Password
        if (line.substr(0, 9) != "Password="){
            std::cerr << "Item format issue. Not saved." << std::endl; return nullptr;
        }
        auto new_password = line.substr(9);

        getline(tmpfile_in, line); // Notes
        if (line != "Notes:"){
            std::cerr << "Item format issue. Not saved." << std::endl; return nullptr;
        }

        // process notes
        std::string new_notes;
        while(getline(tmpfile_in, line)){
            new_notes.append(line).append("\n");
        }

        new_item = new Login(new_name, new_username, new_password, new_notes);

    }else{ // note
        getline(tmpfile_in, line); // Note:
        if (line != "Note:"){
            std::cerr << "Item format issue. Not saved." << std::endl; return nullptr;
        }

        // process lines
        std::string new_note;
        while(getline(tmpfile_in, line)){
            new_note.append(line).append("\n");
        }

        new_item = new Note(new_name, new_note);

    }

    // close and delete
    tmpfile_in.close();
    remove(path.c_str());

    return new_item;

}

