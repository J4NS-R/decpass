
#include <fstream>
#include <sqlite3.h>
#include "DatabaseManager.h"
#include "../constants.h"
#include "../util/IOHelper.h"
#include <sys/stat.h>

// app_dir passed by value, for safe internal manipulation
DatabaseManager::DatabaseManager(std::string app_dir) {

    db_file = app_dir.append("/").append(constants::DB_NAME);

    // opening of db connection happens later

}

DatabaseManager::~DatabaseManager(){
    sqlite3_close_v2(db);
}

// -1 if not present locally
long DatabaseManager::localLastUpdated() {

    if (IOHelper::PathExists(&db_file)){
        return IOHelper::LastUpdated(&db_file);
    }else{
        return -1;
    }

}

/**
 * Create fresh local database file, and closes the connection.
 * Check for `error` afterwards.
 */
void DatabaseManager::createLocalDB() {

    int err = sqlite3_open(db_file.c_str(), &db);
    if (err){
        error = "Failed to create db - code: " + std::to_string(err);
        return;
    }

    auto sql = "CREATE TABLE logins(" \
            "name VARCHAR(255) PRIMARY KEY NOT NULL," \
            "username TEXT," \
            "password TEXT," \
            "notes TEXT); " \
            "CREATE TABLE notes(" \
            "name VARCHAR(255) PRIMARY KEY NOT NULL," \
            "note TEXT); ";
    err = sqlite3_exec(db, sql, nullptr, nullptr, nullptr);
    if (err){
        error = "Failed to set up db - code: " + std::to_string(err);
        return;
    }

    sqlite3_close_v2(db);
    // set perms to be user-mod only
    chmod(db_file.c_str(), S_IRUSR | S_IWUSR);

}

/**
 * Connect to local database file. Has to exist first.
 */
void DatabaseManager::connectLocalDB() {
    int err = sqlite3_open(db_file.c_str(), &db);
    if (err){
        error = "Failed to create db - code: " + std::to_string(err);
        sqlite3_close_v2(db);
        return;
    }
    // closing of db happens at destruction time
}

/**
 * @return The number of logins, and the number of notes
 */
std::tuple<int, int> DatabaseManager::getStats() {

    auto sql = "SELECT " \
        "(SELECT COUNT(name) FROM logins) AS logins, " \
        "(SELECT COUNT(name) FROM notes) AS notes";

    sqlite3_stmt *stmt;
    int err = sqlite3_prepare_v3(db, sql, -1, 0, &stmt, nullptr);
    if (err){
        error = "Failed to get statistics - code: " + std::to_string(err);
        sqlite3_finalize(stmt);
        return std::tuple<int, int>(-1, -1);
    }

    sqlite3_step(stmt);
    int logins = sqlite3_column_int(stmt, 0);
    int notes = sqlite3_column_int(stmt, 1);

    sqlite3_finalize(stmt);
    return std::tuple<int, int>(logins, notes);

}

std::vector<SecureItem*> DatabaseManager::searchAll(std::string &query) {
    // === Logins ===

    // have to manually inject to set up search query properly
    std::string sql = "SELECT name, username, password, notes FROM logins " \
            "WHERE name LIKE '%"+query+"%' " \
            "OR username LIKE '%"+query+"%' " \
            "OR notes LIKE '%"+query+"%' " \
            "ORDER BY name ASC";
    sqlite3_stmt *stmt;
    int err = sqlite3_prepare_v3(db, sql.c_str(), -1, 0, &stmt, nullptr);
    if (err){
        error = "Failed to search logins - code: " + std::to_string(err);
        sqlite3_finalize(stmt);
        return std::vector<SecureItem*>();
    }

    std::vector<SecureItem*> items;
    while(sqlite3_step(stmt) == SQLITE_ROW){
        auto *login = new Login(
                (char*) sqlite3_column_text(stmt, 0), // name
                (char*) sqlite3_column_text(stmt, 1), // username
                (char*) sqlite3_column_text(stmt, 2), // password
                (char*) sqlite3_column_text(stmt, 3) // notes
                );
        items.push_back(login);
    }

    sqlite3_finalize(stmt);

    // === Notes ===

    sql = "SELECT name, note FROM notes " \
            "WHERE name LIKE '%"+query+"%' " \
            "OR note LIKE '%"+query+"%' " \
            "ORDER BY name ASC";
    err = sqlite3_prepare_v3(db, sql.c_str(), -1, 0, &stmt, nullptr);
    if (err){
        error = "Failed to search notes - code: " + std::to_string(err);
        sqlite3_finalize(stmt);
        return std::vector<SecureItem*>();
    }

    while(sqlite3_step(stmt) == SQLITE_ROW){
        auto *note = new Note(
                (char*) sqlite3_column_text(stmt, 0), // name
                (char*) sqlite3_column_text(stmt, 1) // note
                );
        items.push_back(note);
    }

    sqlite3_finalize(stmt);

    return items;
}

std::vector<Login*> DatabaseManager::getAllLogins() {

    auto sql = "SELECT name, username, password, notes FROM logins " \
            "ORDER BY name ASC";
    sqlite3_stmt *stmt;
    int err = sqlite3_prepare_v3(db, sql, -1, 0, &stmt, nullptr);
    if (err){
        error = "Failed to get all logins - code: " + std::to_string(err);
        sqlite3_finalize(stmt);
        return std::vector<Login*>();
    }

    std::vector<Login*> logins;
    while(sqlite3_step(stmt) == SQLITE_ROW){
        auto *login = new Login(
                (char*) sqlite3_column_text(stmt, 0), // name
                (char*) sqlite3_column_text(stmt, 1), // username
                (char*) sqlite3_column_text(stmt, 2), // password
                (char*) sqlite3_column_text(stmt, 3) // notes
        );
        logins.push_back(login);
    }

    sqlite3_finalize(stmt);

    return logins;

}

std::vector<Note*> DatabaseManager::getAllNotes() {
    auto sql = "SELECT name, note FROM notes " \
            "ORDER BY name ASC";
    sqlite3_stmt *stmt;
    int err = sqlite3_prepare_v3(db, sql, -1, 0, &stmt, nullptr);
    if (err){
        error = "Failed to get all notes - code: " + std::to_string(err);
        sqlite3_finalize(stmt);
        return std::vector<Note*>();
    }

    std::vector<Note*> notes;
    while(sqlite3_step(stmt) == SQLITE_ROW){
        auto *note = new Note(
                (char*) sqlite3_column_text(stmt, 0), // name
                (char*) sqlite3_column_text(stmt, 1) // note
        );
        notes.push_back(note);
    }

    sqlite3_finalize(stmt);

    return notes;
}

Login* DatabaseManager::getLogin(const std::string &name) {

    auto sql = "SELECT username, password, notes FROM logins " \
            "WHERE name = ?";
    sqlite3_stmt *stmt;
    int err = sqlite3_prepare_v3(db, sql, -1, 0, &stmt, nullptr);
    if (err){
        error = "Failed to lookup login - code: " + std::to_string(err);
        sqlite3_finalize(stmt);
        return nullptr;
    }

    sqlite3_bind_text(stmt, /*param id*/1, name.c_str(), /*strlen*/-1, nullptr);

    Login *login;
    if (sqlite3_step(stmt) == SQLITE_ROW){
        login = new Login(
                name,
                (char *) sqlite3_column_text(stmt, 0),
                (char *) sqlite3_column_text(stmt, 1),
                (char *) sqlite3_column_text(stmt, 2)
        );
    } else { // empty result
        login = nullptr;
    }

    sqlite3_finalize(stmt);
    return login;
}

Note* DatabaseManager::getNote(const std::string &name) {

    auto sql = "SELECT note FROM notes " \
            "WHERE name = ?";
    sqlite3_stmt *stmt;
    int err = sqlite3_prepare_v3(db, sql, -1, 0, &stmt, nullptr);
    if (err){
        error = "Failed to lookup note - code: " + std::to_string(err);
        sqlite3_finalize(stmt);
        return nullptr;
    }

    sqlite3_bind_text(stmt, /*param id*/1, name.c_str(), /*strlen*/-1, nullptr);

    Note *note;
    if(sqlite3_step(stmt) == SQLITE_ROW) {
        note = new Note(
                name,
                (char *) sqlite3_column_text(stmt, 0)
        );
    }else{ // empty result
        note = nullptr;
    }

    sqlite3_finalize(stmt);
    return note;
}

const std::string &DatabaseManager::getDbFile() {
    return db_file;
}

/**
 * Retrieves a copy of the last error and then clears it.
 * If you just want to check if there is an error (without clearing it), use isError();
 */
std::string DatabaseManager::getError() {
    std::string err_copy = error;
    error = std::string("");
    return err_copy;
}

void DatabaseManager::storeNewItem(SecureItem *item) {
    // check if name already taken
    if (isNameTaken(item->getType(), item->getName())){
        error = "Name \"" + item->getName() + "\" already taken.";
        return;
    }

    sqlite3_stmt *stmt;

    if (item->getType() == SecureItem::ITEM_TYPE::LOGIN){
        auto *login = (Login*) item;

        auto sql = "INSERT INTO logins (name, username, password, notes) " \
                "VALUES (?, ?, ?, ?)";
        int err = sqlite3_prepare_v3(db, sql, -1, 0, &stmt, nullptr);
        if (err){
            error = "Error storing new login - code: " + std::to_string(err); return;
        }

        sqlite3_bind_text(stmt, 1, item->getName().c_str(), -1, nullptr);
        sqlite3_bind_text(stmt, 2, login->getUsername().c_str(), -1, nullptr);
        sqlite3_bind_text(stmt, 3, login->getPassword().c_str(), -1, nullptr);
        sqlite3_bind_text(stmt, 4, login->getNotes().c_str(), -1, nullptr);

    }else{ // note
        auto *note = (Note*) item;

        auto sql = "INSERT INTO notes (name, note) " \
                "VALUES (?, ?)";
        int err = sqlite3_prepare_v3(db, sql, -1, 0, &stmt, nullptr);
        if (err){
            error = "Error storing new note - code: " + std::to_string(err); return;
        }

        sqlite3_bind_text(stmt, 1, item->getName().c_str(), -1, nullptr);
        sqlite3_bind_text(stmt, 2, note->getNote().c_str(), -1, nullptr);

    }

    sqlite3_step(stmt); // exec
    sqlite3_finalize(stmt);

}

void DatabaseManager::updateItem(const std::string &oldname, SecureItem *item) {
    // check if name already taken
    if (isNameTaken(item->getType(), item->getName())){
        error = "New name \"" + item->getName() + "\" already taken.";
        return;
    }

    sqlite3_stmt *stmt;

    if (item->getType() == SecureItem::ITEM_TYPE::LOGIN){
        auto *login = (Login*) item;

        auto sql = "UPDATE logins SET " \
                "name = ?, " \
                "username = ?, " \
                "password = ?, " \
                "notes = ? " \
                "WHERE name = ?";
        int err = sqlite3_prepare_v3(db, sql, -1, 0, &stmt, nullptr);
        if (err){
            error = "Error updating login - code: " + std::to_string(err); return;
        }

        sqlite3_bind_text(stmt, 1, item->getName().c_str(), -1, nullptr);
        sqlite3_bind_text(stmt, 2, login->getUsername().c_str(), -1, nullptr);
        sqlite3_bind_text(stmt, 3, login->getPassword().c_str(), -1, nullptr);
        sqlite3_bind_text(stmt, 4, login->getNotes().c_str(), -1, nullptr);
        sqlite3_bind_text(stmt, 5, oldname.c_str(), -1, nullptr);

    }else{ // note
        auto *note = (Note*) item;

        auto sql = "UPDATE notes SET " \
                "name = ?, " \
                "note = ? " \
                "WHERE name = ?";
        int err = sqlite3_prepare_v3(db, sql, -1, 0, &stmt, nullptr);
        if (err){
            error = "Error storing new note - code: " + std::to_string(err); return;
        }

        sqlite3_bind_text(stmt, 1, item->getName().c_str(), -1, nullptr);
        sqlite3_bind_text(stmt, 2, note->getNote().c_str(), -1, nullptr);
        sqlite3_bind_text(stmt, 3, oldname.c_str(), -1, nullptr);

    }

    sqlite3_step(stmt); // exec
    sqlite3_finalize(stmt);

}

void DatabaseManager::deleteLogin(const std::string &name) {

    auto sql = "DELETE FROM logins WHERE name = ?";
    sqlite3_stmt *stmt;
    int err = sqlite3_prepare_v3(db, sql, -1, 0, &stmt, nullptr);
    if (err){
        error = "Error deleting login - code: " + std::to_string(err); return;
    }

    sqlite3_bind_text(stmt, 1, name.c_str(), -1, nullptr);

    sqlite3_step(stmt); // exec
    sqlite3_finalize(stmt);

}

void DatabaseManager::deleteNote(const std::string &name) {

    auto sql = "DELETE FROM notes WHERE name = ?";
    sqlite3_stmt *stmt;
    int err = sqlite3_prepare_v3(db, sql, -1, 0, &stmt, nullptr);
    if (err){
        error = "Error deleting note - code: " + std::to_string(err); return;
    }

    sqlite3_bind_text(stmt, 1, name.c_str(), -1, nullptr);

    sqlite3_step(stmt); // exec
    sqlite3_finalize(stmt);

}

/**
 * Close connection to the local db file.
 * Should be called before overwriting the db file.
 * Call connectLocalDB() afterwards.
 */
void DatabaseManager::closeDB() {
    // if db is nullptr, this is a no-op
    sqlite3_close_v2(db);
}

/**
 * Checks if SecureItem with said name already exists in db.
 * @param login true if login, otherwise note
 * @param name name to be searched
 * @return bool
 */
bool DatabaseManager::isNameTaken(const SecureItem::ITEM_TYPE login, const std::string &name) {
    std::string sql;
    if (login == SecureItem::ITEM_TYPE::LOGIN){
        sql = "SELECT name FROM logins WHERE name = ?";
    }else{ // note
        sql = "SELECT name FROM notes WHERE name = ?";
    }
    sqlite3_stmt *stmt;
    int err = sqlite3_prepare_v3(db, sql.c_str(), -1, 0, &stmt, nullptr);
    if (err){
        error = "Failed to lookup secure item - code: " + std::to_string(err);
        sqlite3_finalize(stmt);
        return false;
    }

    sqlite3_bind_text(stmt, /*param id*/1, name.c_str(), /*strlen*/-1, nullptr);

    bool record_exists = sqlite3_step(stmt) == SQLITE_ROW;
    sqlite3_finalize(stmt);
    return record_exists;
}
