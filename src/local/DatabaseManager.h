
#ifndef DECPASS_DATABASEMANAGER_H
#define DECPASS_DATABASEMANAGER_H
#include <string>
#include <tuple>
#include <sqlite3.h>
#include <vector>
#include "../data/SecureItem.h"
#include "../data/Login.h"
#include "../data/Note.h"

class DatabaseManager {
public:
    explicit DatabaseManager(std::string app_dir);
    ~DatabaseManager();

    long localLastUpdated();

    void createLocalDB();
    void connectLocalDB();
    void closeDB();

    std::tuple<int, int> getStats();
    std::vector<SecureItem*> searchAll(std::string &query);
    std::vector<Login*> getAllLogins();
    std::vector<Note*> getAllNotes();

    Login* getLogin(const std::string &name);
    Note* getNote(const std::string &name);

    void storeNewItem(SecureItem *item);
    void updateItem(const std::string &oldname, SecureItem *item);

    void deleteLogin(const std::string &name);
    void deleteNote(const std::string &name);

    bool isNameTaken(SecureItem::ITEM_TYPE login, const std::string &name);

private:
    std::string error;
    std::string db_file;
    sqlite3 *db = nullptr;

public: // getters and setters
    [[nodiscard]] const std::string &getDbFile();

    [[nodiscard]] std::string getError();
    [[nodiscard]] bool isError() {
        return !error.empty();
    }

};


#endif //DECPASS_DATABASEMANAGER_H
