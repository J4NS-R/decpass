
#ifndef DECPASS_LOGIN_H
#define DECPASS_LOGIN_H
#include <string>
#include "SecureItem.h"


class Login : public SecureItem {
public:
    Login(const std::string &name, const std::string &username, const std::string &password, const std::string &notes)
            : SecureItem(name), username(username), password(password), notes(notes) {
        itemType = ITEM_TYPE::LOGIN;
    }

private:
    std::string username;
    std::string password;
    std::string notes;
public:
    const std::string &getUsername() const {
        return username;
    }

    const std::string &getPassword() const {
        return password;
    }

    const std::string &getNotes() const {
        return notes;
    }
};


#endif //DECPASS_LOGIN_H
