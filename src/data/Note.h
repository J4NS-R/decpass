
#ifndef DECPASS_NOTE_H
#define DECPASS_NOTE_H
#include <string>
#include "SecureItem.h"

class Note : public SecureItem{
public:
    Note(const std::string &name, const std::string &note) : SecureItem(name), note(note) {
        itemType = ITEM_TYPE::NOTE;
    }

private:
    std::string note;

public:
    const std::string &getNote() const {
        return note;
    }
};

#endif //DECPASS_NOTE_H
