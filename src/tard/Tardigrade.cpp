
#include "Tardigrade.h"
#include <uplink/uplink.h>
#include <iostream>
#include <fstream>
#include "../constants.h"
#include <sys/stat.h>

Tardigrade::Tardigrade(const char* access_grant, const char* the_bucket) {
    UplinkAccessResult access_res = uplink_parse_access(access_grant);
    if (access_res.error) {
        error = access_res.error->message;
        return;
    }

    UplinkProjectResult proj_res = uplink_open_project(access_res.access);
    if (proj_res.error) {
        error = proj_res.error->message;
        return;
    }
    proj = proj_res.project;

    // check if bucket exists
    UplinkBucketResult bucket_res = uplink_stat_bucket(proj_res.project, the_bucket);
    if (bucket_res.error){
        error = bucket_res.error->message;
        return;
    }

    bucket = the_bucket;

    uplink_free_bucket_result(bucket_res);
    uplink_free_access_result(access_res);
    // keeping proj_res for `proj` member
}

Tardigrade::~Tardigrade() {
    uplink_close_project(proj);
}

// Check when last the remote db was updated (in unix time)
// -1 means object not found; -2 means other error
long Tardigrade::dbLastUpdated() {
    UplinkObjectResult objres = uplink_stat_object(proj, bucket.c_str(), constants::DB_NAME);
    if (objres.error){
        if (objres.error->code == 33){
            return -1; // not present remotely
        }else { // unknown error
            std::cerr << objres.error->message << std::endl;
            return -2;
        }
    }

    return objres.object->system.created;

}

// note: will overwrite remote file
void Tardigrade::upload(const string &localfile, const string &remotefile) {

    UplinkUploadResult upres = uplink_upload_object(proj, bucket.c_str(), remotefile.c_str(), nullptr);
    if (upres.error){
        error = upres.error->message;
        return;
    }

    ifstream filestream(localfile, ios::in | ios::binary);
    if (!filestream){
        error = "Could not open file for reading: " + localfile;
        return;
    }

    char buf[constants::BIN_BUFF];
    while(!filestream.eof()){
        filestream.read(buf, constants::BIN_BUFF);
        int bytes_read = filestream.gcount();
        // todo live progress
        UplinkWriteResult writeRes = uplink_upload_write(upres.upload, buf, bytes_read);
        if (writeRes.error){ // todo retry upload failures
            error = writeRes.error->message;
            return;
        }
    }
    filestream.close();

    UplinkError *commitErr = uplink_upload_commit(upres.upload);

    uplink_free_upload_result(upres);

    if (commitErr){
        error = commitErr->message;
        return;
    }

}

/**
 * Download and overwrite local file.
 * @param safePerms if true will set local file perms to 600.
 */
void Tardigrade::download(const string &remotefile, const string &localfile, bool safePerms) {

    auto downres = uplink_download_object(proj, bucket.c_str(), remotefile.c_str(), nullptr);
    if (downres.error){
        error = downres.error->message;
        return;
    }

    ofstream filestream(localfile, ios::out | ios::binary);
    if (!filestream){
        error = "Could not open file for writing: " + localfile;
        return;
    }

    char buf[constants::BIN_BUFF];
    UplinkReadResult dlres;

    do {
        dlres = uplink_download_read(downres.download, buf, constants::BIN_BUFF);
        if (dlres.error && dlres.error->code > 0){ // code -1 means no more bytes (download complete)
            error = dlres.error->message;
            return;
        } // todo retry download failures

        if (dlres.bytes_read > 0)
            filestream.write(buf, dlres.bytes_read);

    }while (dlres.bytes_read > 0);

    filestream.close();
    uplink_free_download_result(downres);

    if (safePerms){
        chmod(localfile.c_str(), S_IRUSR | S_IWUSR);
    }
}
