
#include "Synchroniser.h"
#include <iostream>
#include "../constants.h"

using namespace std;

/**
 * These passed in values are not managed by this class, only used.
 */
Synchroniser::Synchroniser(DatabaseManager *db, Tardigrade *tard, ConfigManager *cfg) {
    Synchroniser::db = db;
    Synchroniser::tard = tard;
    Synchroniser::cfg = cfg;
}

/**
 * Synchronise the database with remote.
 * 0 = success
 * 1 = failure (will print to cerr)
 */
int Synchroniser::sync(){
    long remoteUpdateTime = tard->dbLastUpdated();
    long localUpdateTime = db->localLastUpdated();

    bool shouldUpload = false, shouldDownload = false;

    if (remoteUpdateTime == -1){ // not present remotely
        cout << "Database is not present remotely." << endl;
        shouldUpload = true;

        if (localUpdateTime == -1){  // not present locally
            cout << "Creating database locally..." << endl;
            db->createLocalDB();
            localUpdateTime = db->localLastUpdated();
        }

    }else{ // db present remotely

        long remoteSyncTime = cfg->getRemoteSyncTs();
        long localSyncTime = cfg->getLocalSyncTs();

        if (localUpdateTime == -1 || remoteUpdateTime > remoteSyncTime){ // local missing or remote newer
            shouldDownload = true;

        }else if (localUpdateTime > localSyncTime){ // local newer
            shouldUpload = true;

        }

    }

    // actual up/down ===

    db->closeDB(); // to ensure no corruption during transfer

    if (shouldUpload) {
        cout << "Uploading local database..." << endl;
        tard->upload(db->getDbFile(), constants::DB_NAME);
        if (!tard->error.empty()) {
            cerr << tard->error << endl;
            return 1;
        }
    }else if (shouldDownload) {
        cout << "Fetching remote database (newer)..." << endl;
        tard->download(constants::DB_NAME, db->getDbFile(), true);
        if (!tard->error.empty()) {
            cerr << tard->error << endl;
            return 1;
        }
    }

    if (shouldUpload || shouldDownload) {
        // log what the up-to-date dates are only if there was a change
        cfg->setRemoteSyncTs(tard->dbLastUpdated());
        cfg->setLocalSyncTs(db->localLastUpdated());
        cfg->writeOut();
    }

    db->connectLocalDB(); // reconnect

    cout << "Sync complete." << endl;
    return 0;
}
