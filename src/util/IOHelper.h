
#ifndef DECPASS_IOHELPER_H
#define DECPASS_IOHELPER_H

#include <string>

class IOHelper {
public:
    static bool PathExists(const std::string *path);
    static long LastUpdated(const std::string *path);
};


#endif //DECPASS_IOHELPER_H
