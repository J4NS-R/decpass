
#include "Repl.h"
#include <iostream>
#include <string>
#include "GeneralHelper.h"
#include "../local/Editor.h"

using namespace std;

// constructor shorthand
Repl::Repl(DatabaseManager *db, Synchroniser *sync) : db(db), sync(sync) {}

void Repl::run() {

    std::string selection; char cmd; std::string arg;
    bool show_help = false;
    cout << "REPL ready. Press [?] for help" << endl;
    while (true) {

        cout << ">> ";
        getline(cin, selection);
        if (selection.length() > 0){
            cmd = selection.at(0);
            if (selection.length() > 2)
                arg = selection.substr(2);
            else
                arg = "";
        }else continue; // ignore blank line

        if (cmd == 'q') { // quit
            sync->sync();
            break;
        }else if (cmd == '?' || cmd == 'h') { // help
            cout << "Help:" << endl
                 << "  [t] - get some statistics" << endl
                 << "  [s <query>] - search everything (omit query to list all)" << endl
                 << "  [l <name>] - edit/create login" << endl
                 << "  [n <name>] - edit/create note" << endl
                 << "  [d <name>] - delete item" << endl
                 << "  [y] - perform sync" << endl
                 << "  [q] - save & quit" << endl;

        }else if (cmd == 't'){ // stats
            tuple<int, int> stats = db->getStats();
            if (db->isError()){
                cerr << db->getError() << endl; break;
            }
            cout << "Stats: " << get<0>(stats) << " logins and " << get<1>(stats) << " notes." << endl;

        }else if (cmd == 'y'){ // sync
            sync->sync();

        }else if (cmd == 's'){ // search
            search(arg);

        }else if (cmd == 'l'){ // modify login
            modify(true, arg);

        }else if (cmd == 'n') { // modify note
            modify(false, arg);

        }else if (cmd == 'd') { // delete
            deleteItem(arg);

        }else{
            cout << "Unrecognised option. Press [?] for help." << endl;
        }

    }

}

void Repl::search(std::string &query) {

    if (query.empty()){ // list all
        vector<Login*> logins = db->getAllLogins();
        if (db->isError()){
            cerr << db->getError() << endl;
            return;
        }
        cout << "=== Logins ===" << endl;
        for(const Login *login : logins){
            cout << "Login: " << login->getName() << endl
                 << "  Username: " << login->getUsername() << endl
                 << endl;
        }

        auto notes = db->getAllNotes();
        if (db->isError()){
            cerr << db->getError() << endl;
            return;
        }
        cout << "=== Notes ===" << endl;
        for (const Note *note : notes){
            cout << "Note: " << note->getName() << endl
                << endl;
        }

        cout << "Listing done. Add a search query to get more info per item." << endl;

    }else{ // perform a search
        vector<SecureItem*> items = db->searchAll(query);
        if (db->isError()){
            cerr << db->getError() << endl;
            return;
        }
        cout << "=== Search results (" << items.size() << ") ===" << endl;

        for(SecureItem *item : items){

            if(item->getType() == SecureItem::ITEM_TYPE::LOGIN){
                auto *login = (Login*) item;
                cout << "Login: " << login->getName() << endl
                     << "  Username: " << login->getUsername() << endl
                     << "  Password: " << login->getPassword() << endl
                     << "  Notes:" << endl;

                // read out notes
                GeneralHelper::printLines(login->getNotes(), 4);

            }else{
                auto *note = (Note*) item;
                cout << "Note: " << note->getName() << endl;
                GeneralHelper::printLines(note->getNote(), 2);

            }

            cout << "======" << endl;

        }

    }

}

/**
 *
 * @param login true if modifying a login, false if modifying a note
 * @param name of the secure item
 */
void Repl::modify(bool login, std::string &name) {

    SecureItem *item;
    if (login){
        item = db->getLogin(name);
    }else{
        item = db->getNote(name);
    }

    if (db->isError()){
        cerr << db->getError() << endl; return;
    }

    SecureItem *newitem;
    if (item == nullptr){ // new
        newitem = Editor::newItem(login, name);
        if (newitem == nullptr) return;

        db->storeNewItem(newitem);

    }else{ // modify
        newitem = Editor::editItem(item);
        if (newitem == nullptr) return;

        db->updateItem(name, newitem);

    }

    if (db->isError()){
        cerr << "Error adding new item: " << db->getError() << endl;
        return;
    }

    cout << "Item \"" << newitem->getName() << "\" saved." << endl;

}

void Repl::deleteItem(const std::string &name) {

    auto *note = db->getNote(name);
    auto *login = db->getLogin(name);

    if (db->isError()){
        cerr << "Error looking up item for deletion: " << db->getError() << endl; return;
    }

    if (note != nullptr){ // delete note
        db->deleteNote(name);
    }else if (login != nullptr) { // delete login
        db->deleteLogin(name);
    }else{
        cout << "Could not find item \"" << name << "\". Check for typos." << endl;
        return;
    }

    if (db->isError()){
        cerr << "Error deleting item: " << db->getError() << endl; return;
    }

    cout << "Item \"" << name << "\" deleted." << endl;

}


