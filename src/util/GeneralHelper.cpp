
#include "GeneralHelper.h"
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>


// Prints either to cout or the filestream
void GeneralHelper::printLines(const std::string &content, short indentation, std::ofstream *filestream){
    std::stringstream ss(content);
    std::string line;
    while (getline(ss, line)){
        for(short i = 0; i < indentation; i++){
            if (filestream == nullptr)
                std::cout << " ";
            else
                *filestream << " ";
        }
        if (filestream == nullptr)
            std::cout << line << std::endl;
        else
            *filestream << line << std::endl;
    }
}
